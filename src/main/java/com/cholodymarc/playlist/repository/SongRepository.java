package com.cholodymarc.playlist.repository;

import com.cholodymarc.playlist.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepository extends JpaRepository<Song, Long> {

}
