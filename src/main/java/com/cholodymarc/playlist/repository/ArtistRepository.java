package com.cholodymarc.playlist.repository;

import com.cholodymarc.playlist.model.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<Artist, Long> {

}
