package com.cholodymarc.playlist.repository;

import com.cholodymarc.playlist.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepository extends JpaRepository<Album, Long> {

}
