package com.cholodymarc.playlist.service;

import com.cholodymarc.playlist.exception.ArtistNotFoundException;
import com.cholodymarc.playlist.model.Artist;
import com.cholodymarc.playlist.repository.ArtistRepository;
import org.springframework.stereotype.Service;

@Service
public class ArtistService {
    private ArtistRepository artistRepository;

    public ArtistService(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    public Artist getArtistById(Long id) {
        return artistRepository
                .findById(id)
                .orElseThrow(() -> new ArtistNotFoundException("missing artist id: " + id));
    }

    public Artist createNewArtist(Artist artist) {
        return artistRepository.save(artist);
    }

    public void deleteArtistById(Long id) {
        Artist artistToDelete = artistRepository
                .findById(id)
                .orElseThrow(() -> new ArtistNotFoundException("missing artist id: " + id));
        artistRepository.delete(artistToDelete);
    }

    public Artist updateArtistById(Artist updatedArtist, Long id) {
        return artistRepository.findById(id)
                .map(artist -> {
                    artist.setName(updatedArtist.getName());
                    return artistRepository.save(artist);
                })
                .orElseGet(() -> {
                    updatedArtist.setId(id);
                    return artistRepository.save(updatedArtist);
                });
    }
}
