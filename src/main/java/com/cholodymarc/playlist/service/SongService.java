package com.cholodymarc.playlist.service;

import com.cholodymarc.playlist.exception.SongNotFoundException;
import com.cholodymarc.playlist.model.Song;
import com.cholodymarc.playlist.model.SongsDto;
import com.cholodymarc.playlist.repository.SongRepository;
import org.springframework.stereotype.Service;

@Service
public class SongService {

    private SongRepository songRepository;

    public SongService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    public SongsDto getAllSongs() {
        return new SongsDto(songRepository.findAll());
    }

    public Song getSongById(Long id) {
        return songRepository
                .findById(id)
                .orElseThrow(() -> new SongNotFoundException("missing song id: " + id));
    }

    public Song creteNewSong(Song song) {
        return songRepository.save(song);
    }

    public void deleteSongById(Long id) {
        Song song = songRepository
                .findById(id)
                .orElseThrow(() -> new SongNotFoundException("missing song id: " + id));
        songRepository.delete(song);
    }

    public Song updateSongById(Song updatedSong, Long id) {
        return songRepository.findById(id)
                .map(song -> {
                    song.setTitle(updatedSong.getTitle());
                    return songRepository.save(song);
                })
                .orElseGet(() -> {
                    updatedSong.setId(id);
                    return songRepository.save(updatedSong);
                });
    }
}