package com.cholodymarc.playlist.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}test").roles("USER");
        auth.inMemoryAuthentication()
                .withUser("superuser").password("{noop}supertest").roles("USER", "ADMIN");
    }
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/songs/{id}").hasAnyAuthority("ROLE_USER")
                .antMatchers(HttpMethod.GET, "/songs").hasAnyAuthority("ROLE_USER")
                .anyRequest()
                .permitAll()
                .and()
                .csrf()
                .disable()
                .formLogin();
        http.headers().frameOptions().sameOrigin();
    }
}

