package com.cholodymarc.playlist.exception;

public class SongNotFoundException extends RuntimeException{

    public SongNotFoundException(String message){
        super(message);
    }
}
