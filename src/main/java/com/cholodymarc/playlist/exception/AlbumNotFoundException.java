package com.cholodymarc.playlist.exception;

public class AlbumNotFoundException extends RuntimeException {
    public  AlbumNotFoundException(String message){
        super(message);
    }
}
