package com.cholodymarc.playlist.controller;

import com.cholodymarc.playlist.model.Artist;
import com.cholodymarc.playlist.service.ArtistService;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ArtistController {
    private ArtistService artistService;

    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @GetMapping("/artists/{id}")
    public EntityModel<Artist> getArtistById(@PathVariable("id") Long id){
        return new EntityModel<>(artistService.getArtistById(id),
                linkTo(methodOn(ArtistController.class).getArtistById(id)).withSelfRel());
    }

    @GetMapping("/artists")
    public ArtistsDto getAllArtists(){
        return artistService.getAllArtists();
    }

    @PostMapping("/artists")
    public Artist createArtist(@RequestBody @Valid Artist artist){
        return artistService.createNewArtist(artist);
    }

    @DeleteMapping("/artists/{id}")
    public void deleteArtistById(@PathVariable Long id){
        artistService.deleteArtistById(id);
    }

    @PutMapping("artists/{id}")
    public Artist updateArtistById(@RequestBody Artist updatedArtist, @PathVariable Long id){
        return artistService.updateArtistById(updatedArtist, id);
    }
}
