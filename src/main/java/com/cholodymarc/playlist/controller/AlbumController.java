package com.cholodymarc.playlist.controller;

import com.cholodymarc.playlist.model.Album;
import com.cholodymarc.playlist.model.Song;
import com.cholodymarc.playlist.model.SongsDto;
import com.cholodymarc.playlist.service.AlbumService;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class AlbumController {
    private AlbumService albumService;

    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping("/albums/{id}")
    public EntityModel<Album> getAlbumById(@PathVariable("id") Long id) {
        return new EntityModel<>(albumService.getAlbumById(id),
                linkTo(methodOn(AlbumController.class).getAlbumById(id)).withSelfRel());
    }

    @GetMapping("/albums")
    public AlbumsDto getAllAlbums() {
        return albumService.getAllAlbums();
    }

    @PostMapping("/albums")
    public Album createAlbum(@RequestBody @Valid Album album) {
        return albumService.creteNewAlbum(album);
    }

    @DeleteMapping("/albums/{id}")
    public void deleteAlbumById(@PathVariable Long id) {
        albumService.deleteAlbumById(id);
    }

    @PutMapping("/albums/{id}")
    public Album updateAlbumById(@RequestBody Album updatedAlbum, @PathVariable Long id) {
        return albumService.updateAlbumById(updatedAlbum, id);
    }
}
