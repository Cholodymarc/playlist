package com.cholodymarc.playlist.controller;

import com.cholodymarc.playlist.model.Song;
import com.cholodymarc.playlist.model.SongsDto;
import com.cholodymarc.playlist.service.SongService;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class SongController {

    private SongService songService;

    public SongController(SongService songService) {
        this.songService = songService;
    }

    @GetMapping("/songs/{id}")
    public EntityModel<Song> getSongById(@PathVariable("id") Long id) {
        return new EntityModel<>(songService.getSongById(id),
                linkTo(methodOn(SongController.class).getSongById(id)).withSelfRel());
    }

    @GetMapping("/songs")
    public SongsDto getAllSongs() {
        return songService.getAllSongs();
    }

    @PostMapping("/songs")
    public Song createSong(@RequestBody @Valid Song song) {
        return songService.creteNewSong(song);
    }

    @DeleteMapping("/songs/{id}")
    public void deleteSongById(@PathVariable Long id) {
        songService.deleteSongById(id);
    }

    @PutMapping("/songs/{id}")
    public Song updateSongById(@RequestBody Song updatedSong, @PathVariable Long id) {
        return songService.updateSongById(updatedSong, id);
    }
}
