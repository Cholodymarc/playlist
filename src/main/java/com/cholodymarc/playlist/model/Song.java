package com.cholodymarc.playlist.model;

import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;

@Entity
public class Song extends RepresentationModel<Song> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @ManyToOne(fetch = FetchType.LAZY)
    private Album album;
    @ManyToOne(fetch = FetchType.LAZY)
    private Artist artist;

    public Song() {
    }

    public Song(Long id, String name){
        this.title = name;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }
}
