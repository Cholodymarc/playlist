package com.cholodymarc.playlist.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "songs")
public class SongsDto {
    @JacksonXmlProperty(localName = "song")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("songs")
    private List<Song> songs;

    public SongsDto(List<Song> songs) {
        this.songs = songs;
    }

    public List<Song> getSongs(){
        return songs;
    }
}
